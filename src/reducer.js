type StateType = {
  Home: {
    isFetchingFeed: Boolean,
    isFetchingEvent: Boolean,
    event: any,
    eventReq: null,
    eventError: Error | null,
    feed: any,
    feedReq: null,
    feedError: Error | null,
  },
};

type ActionType = {
  type: 'requestHomeFeed' |
    'requestHomeEvent' |
    'receiveHomeFeed' |
    'receiveHomeEvent' |
    'errorHomeFeed' |
    'errorHomeEvent' |
    'requestEvents' |
    'receiveEvents' |
    'errorEvents',
};

export const intialState:StateType = {
  Home: {
    isFetchingEvent: false,
    isFetchingFeed: false,
    event: {},
    eventError: null,
    feed: [],
    feedError: null,
  },
  Events: {
    items: [],
    isFetching: false,
    req: null,
    error: null,
  },
};

const reducer = (state:StateType, action:ActionType) => {
  switch (action.type) {
    case 'requestHomeEvent': {
      const Home = {
        ...state.Home,
        isFetchingEvent: true,
        eventReq: action.req,
      };
      return {...state, Home};
    }
    case 'requestHomeFeed': {
      const Home = {
        ...state.Home,
        isFetchingFeed: true,
        feedReq: action.req,
      };
      return {...state, Home};
    }
    case 'receiveHomeEvent': {
      const Home = {
        ...state.Home,
        isFetchingEvent: false,
        event: action.event,
        eventError: null,
      };
      return {...state, Home};
    }
    case 'receiveHomeFeed': {
      const Home = {
        ...state.Home,
        isFetchingFeed: false,
        feed: action.feed,
        feedError: null,
      };
      return {...state, Home};
    }
    case 'errorHomeEvent': {
      const Home = {
        ...state.Home,
        isFetchingEvent: false,
        eventError: action.error,
      };
      return {...state, Home};
    }
    case 'errorHomeFeed': {
      const Home = {
        ...state.Home,
        isFetchingFeed: false,
        feedError: action.error,
      };
      return {...state, Home};
    }
    case 'requestEvents': {
      const Events = {
        ...state.Events,
        isFetching: true,
        req: action.req,
      };
      return {...state, Events};
    }
    case 'receiveEvents': {
      const Events = {
        ...state.Events,
        isFetching: false,
        items: action.items,
      };
      return {...state, Events};
    }
    case 'errorEvents': {
      const Events = {
        ...state.Events,
        isFetching: false,
        items: action.items,
      };
      return {...state, Events};
    }
  }
};

export default reducer;
