import React from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import HomeScreen from 'src/screen/home';
import AthletesScreen from 'src/screen/athletes';
import EventsScreen from 'src/screen/events';
import NotificationsScreen from 'src/screen/notifications';
import MoreScreen from 'src/screen/more';
import HomeStack from './home';

const AppStack = createBottomTabNavigator({
  HomeStack,
  AthletesScreen,
  EventsScreen,
  NotificationsScreen,
  MoreScreen,
}, {
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;
      let screen = {
        HomeStack: [MaterialCommunityIcons, `home${focused ? '' : '-outline'}`],
        AthletesScreen: [MaterialIcons, `person${focused ? '' : '-outline'}`],
        EventsScreen: [Ionicons, `md-timer`],
        NotificationsScreen: [Ionicons, `ios-notifications${focused ? '' : '-outline'}`],
        MoreScreen: [MaterialIcons, `more-horiz`],
      }[routeName] || [null, null];
      const [Icon, iconName] = screen;
      return Icon ? <Icon name={iconName} size={25} color={tintColor} />:null;
    },
  }),
  tabBarOptions: {
    activeTintColor: 'rgb(234, 83, 102)',
    inactiveTintColor: 'rgb(174, 184, 199)',
  },
});

export default AppStack;
