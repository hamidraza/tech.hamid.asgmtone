import { createStackNavigator } from "react-navigation";
import HomeScreen from "src/screen/home";

const HomeStack = createStackNavigator({
  HomeScreen,
}, {
  cardStyle: {
    backgroundColor: 'rgb(246, 247, 249)',
  },
  defaultNavigationOptions: {
    headerStyle: {
      // elevation: 0,
      // borderBottomColor: 'transparent',
    },
  },
  headerLayoutPreset: 'center',
});

HomeStack.navigationOptions = {
  title: 'Home',
};

export default HomeStack;
