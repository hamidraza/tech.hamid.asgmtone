import { createAppContainer } from "react-navigation";
import AppStack from "./app";

export default createAppContainer(AppStack);
