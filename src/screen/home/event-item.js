import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import FastImage from 'react-native-fast-image';

type Props = {
  creatives: {
    bannerListing: {
      url: String,
    },
  },
  content: {
    name: String,
  },
};

const EventItem = ({
  creatives,
  content,
}: Props) => {
  return (
    <View style={styles.container}>
      <FastImage source={{uri: creatives.bannerListing.url}} resizeMode={FastImage.resizeMode.cover} style={styles.img} />
      <View style={styles.content}>
        <Text>{content.name}</Text>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    // padding: 50,
    margin: 8,
    backgroundColor: '#ffffff',
  },
  img: {
    height: 100,
    width: 200,
  },
  content: {
    padding: 8,
  },
});

export default EventItem;
