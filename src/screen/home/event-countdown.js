import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import { getTimeLeft } from 'src/helpers/date';

type Props = {
  startTime: String,
};

const EventCountdown = ({
  startTime,
}: Props) => {
  let timer;
  const [timeLeft, setTimeLeft] = useState(getTimeLeft((new Date(startTime)).getTime() - new Date));
  useEffect(() => {
    timer = setInterval(() => {
      setTimeLeft(getTimeLeft((new Date(startTime)).getTime() - new Date));
    }, 1000);
    return () => clearInterval(timer);
  }, [startTime]);
  return (
    <View style={styles.container}>
      <View style={styles.timeBox}>
        <Text style={styles.val}>{timeLeft.days}</Text>
        <Text style={styles.lbl}>DAYS</Text>
      </View>
      <View style={styles.timeBox}>
        <Text style={styles.val}>{timeLeft.hours}</Text>
        <Text style={styles.lbl}>HRS</Text>
      </View>
      <View style={styles.timeBox}>
        <Text style={styles.val}>{timeLeft.minutes}</Text>
        <Text style={styles.lbl}>MINS</Text>
      </View>
      <View style={styles.timeBox}>
        <Text style={styles.val}>{timeLeft.seconds}</Text>
        <Text style={styles.lbl}>SECS</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249, 220, 74)',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 8,
    marginBottom: 8,
  },
  timeBox: {
    alignItems: 'center',
    paddingHorizontal: 8,
  },
  lbl: {
    color: '#000000',
  },
  val: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 24,
  },
});

export default EventCountdown;
