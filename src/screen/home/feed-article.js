import React from 'react';
import {View, Text, StyleSheet, Share} from 'react-native';
import FastImage from 'react-native-fast-image';
import { formatDistance } from 'date-fns';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';

const hitSlop = {
  top: 16,
  left: 16,
  right: 16,
  bottom: 16,
};

type Props = {
  link: String,
  description: String,
  id: String,
  category: String,
  title: String,
  featured_image: {
    url: String,
  },
  published_date: String,
  slug: String,
};

const FeedArticle = ({
  link,
  description,
  id,
  category,
  title,
  featured_image,
  published_date,
  slug,
}: Props) => {
  return (
    <TouchableOpacity style={styles.container} onPress={() => alert('goto article')}>
      <FastImage source={{uri: featured_image.url}} style={styles.img} resizeMode={FastImage.resizeMode.cover} />
      <View style={styles.content}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.description}>{decodeURIComponent(description)}</Text>
        <View style={{flex: 1}} />
        <View style={styles.footer}>
          <Text style={styles.timeDistance}>{formatDistance(new Date, new Date(published_date))} ago</Text>
          <TouchableOpacity onPress={() => Share.share({message: description, title, url: link})} hitSlop={hitSlop}>
            <Ionicons name='md-share' size={16} />
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 16,
    marginVertical: 8,
    backgroundColor: '#ffffff',
    borderRadius: 4,
    elevation: 4,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    flexDirection: 'row',
  },
  img: {
    width: 150,
    height: 200,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
  },
  content: {
    padding: 16,
    flex: 1,
  },
  title: {
    fontWeight: 'bold',
    marginBottom: 8,
  },
  description: {
    color: '#00000080',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  timeDistance: {
    fontSize: 12,
    color: '#000000',
  },
});

export default FeedArticle;
