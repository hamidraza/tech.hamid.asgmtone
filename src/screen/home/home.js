import React, {useEffect, useContext} from 'react';
import {View, Text, StyleSheet, ActivityIndicator, FlatList, TouchableOpacity, ScrollView} from 'react-native';
import OneContext from 'src/context';
import { GetFeed, GetEvent, GetEvents } from 'src/api/home';
import EventBanner from './event-banner';
import EventCountdown from './event-countdown';
import FeedArticle from './feed-article';
import FeedYoutube from './feed-youtube';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EventItem from './event-item';
import FastImage from 'react-native-fast-image';

const HomeScreen = () => {
  const {state: {Home, Events}, dispatch} = useContext(OneContext);
  const {
    event, eventReq, isFetchingEvent, eventError,
    feed, feedReq, isFetchingFeed, feedError,
  } = Home;

  const shouldFetchEvent = !eventReq && !isFetchingEvent && !event.id && !eventError;
  useEffect(() => {
    if(shouldFetchEvent) {
      const req = GetEvent()
        .then(res => dispatch({type: 'receiveHomeEvent', event: res.data[0]}))
        .catch(error => dispatch({type: 'errorHomeEvent', error}));
      dispatch({type: 'requestHomeEvent', req});
    }
  });

  const shouldFetchFeed = !feedReq && !isFetchingFeed && !feed.length && !feedError;
  useEffect(() => {
    if(shouldFetchFeed) {
      const req = GetFeed()
        .then(res => dispatch({type: 'receiveHomeFeed', feed: res.data || []}))
        .catch(error => dispatch({type: 'errorHomeFeed', error}));
      dispatch({type: 'requestHomeFeed', req});
    }
  });

  const shouldFetchEvents = !Events.req && !Events.isFetching && !Events.items.length && !Events.error;
  useEffect(() => {
    if(shouldFetchEvents) {
      const req = GetEvents()
        .then(res => dispatch({type: 'receiveEvents', items: res.data || []}))
        .catch(error => dispatch({type: 'errorEvents', error}));
      dispatch({type: 'requestEvents', req});
    }
  });

  if(shouldFetchEvent || shouldFetchFeed || isFetchingEvent || isFetchingFeed) {
    return (
      <View style={{flex: 1, lignItems: 'center', justifyContent: 'center'}}>
        <ActivityIndicator />
      </View>
    );
  };

  const Header = () => (
    <React.Fragment>
      <EventBanner {...event} onPress={() => alert('EVENT')} />
      <EventCountdown {...event} />
      <React.Fragment>
        <View style={styles.eventsTitle}>
          <Text style={styles.eventsTitleText}>FEATURED EVENTS</Text>
        </View>
        <FlatList
          contentContainerStyle={{paddingHorizontal: 8}}
          horizontal={true}
          data={Events.items}
          renderItem={({item}) => <EventItem {...item} />}
          keyExtractor={item => `${item.id}`}
          showsHorizontalScrollIndicator={false}
        />
      </React.Fragment>
    </React.Fragment>
  );

  return (
    <FlatList
      data={feed}
      ListHeaderComponent={Header}
      renderItem={({item}) => (
        item.type == 'YOUTUBE' ?
          <FeedYoutube {...item.data} /> :
          <FeedArticle {...item.data} />
      )}
      keyExtractor={item => item.id}
    />
  );
};

HomeScreen.navigationOptions = {
  headerTitle: (
    <FastImage
      source={require('./assets/title-logo.png')}
      style={{width: 100, height: 30}}
      resizeMode={FastImage.resizeMode.contain}
    />
  ),
  headerRight: (
    <TouchableOpacity style={{paddingHorizontal: 16}}>
      <Ionicons name='ios-search' size={20} />
    </TouchableOpacity>
  ),
};

const styles = StyleSheet.create({
  eventsTitle: {
    paddingHorizontal: 16,
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  eventsTitleText: {
    fontSize: 14,
    color: '#000000',
  },
  eventsTitleViewAll: {
    color: 'rgb(234, 83, 102)',
  },
});

export default HomeScreen;
