import React from 'react';
import {View, Text, Dimensions, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import Image from 'react-native-fast-image';

const screenWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    height: screenWidth/(1500/541),
    overflow: 'hidden',
  },
  banner: {
    width: screenWidth,
    height: screenWidth/(1038/526),
  },
});

type Props = {
  creatives: any,
  onPress: () => void,
};

const EventBanner = ({
  creatives,
  onPress = () => {},
}: Props) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.container}>
        <Image
          source={{uri: creatives.bannerUpcoming.url}}
          resizeMode={Image.resizeMode.cover}
          style={styles.banner}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default EventBanner;
