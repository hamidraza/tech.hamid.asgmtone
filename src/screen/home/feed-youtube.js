import React, {useState} from 'react';
import {Text, View, Dimensions, StyleSheet, TouchableOpacity, Share, TouchableHighlight, Alert} from 'react-native';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/Ionicons';
import formatDistance from 'date-fns/esm/formatDistance'

const screenWidth = Dimensions.get('screen').width;

type Props = {
  playlistId: String,
  link: String,
  description: String,
  athletes: Array<any>,
  id: String,
  title: String,
  featured_image: {
    width: Number,
    url: String,
    height: Number,
  },
  published_date: String,
  slug: String,
};

const FeedYoutube = ({
  playlistId,
  link,
  description,
  athletes = [],
  id,
  title,
  featured_image,
  published_date,
  slug,
}: Props) => {
  const [liked, setLiked] = useState(false);
  const timeDistance = formatDistance(new Date, new Date(published_date));
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={styles.title}>
          <Text style={styles.titleText} numberOfLines={1}>{title}</Text>
          <View style={styles.titleActions}>
            <TouchableOpacity style={styles.titleActionItem} onPress={() => setLiked(!liked)}>
              <Icon name={`ios-heart${!liked?'-empty':''}`} size={20} color={liked ? '#f00':'#000'} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.titleActionItem} onPress={() => Share.share({message: description, title, url:link})}>
              <Icon name={`md-share`} size={20} color={'#000'} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.imgWrap}>
          <FastImage
            source={{uri: featured_image.url}}
            style={{
              width: '100%',
              height: screenWidth/(featured_image.width/featured_image.height),
            }}
          />
          <TouchableHighlight style={styles.playWrap}
            onPress={() => alert('goto details')}
            onLongPress={() => alert('Play preview')}
            underlayColor='#00000080'
          >
            <View style={styles.play}>
              <Icon name='ios-play' size={20} color='#ffffff' />
            </View>
          </TouchableHighlight>
        </View>
        <Text style={styles.description} numberOfLines={2}>{description}</Text>
        <View style={styles.footer}>
          <Text style={styles.timeDistance}>{timeDistance} ago</Text>
          <TouchableOpacity onPress={() => Alert.alert('Show Comments')}>
            <Text style={styles.commentCount}>3 comments</Text>
          </TouchableOpacity>
        </View>
      </View>
      {playlistId ? (
        <View>
          <TouchableOpacity style={{alignItems: 'center'}} onPress={() => Alert.alert('GOTO/Expand', 'video playlist')}>
            <View style={[styles.playlistBar, {width: '90%', zIndex: 2}]} />
            <View style={[styles.playlistBar, {width: '80%', zIndex: 1}]} />
          </TouchableOpacity>
          <View style={{position: 'absolute', alignItems: 'center', zIndex: 4, top: 7, left: 0, right: 0}} pointerEvents='none'>
            <Text style={{color: '#00000080', backgroundColor: '#ffffff'}}>More Videos</Text>
          </View>
        </View>
      ):null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 8,
  },
  content: {
    backgroundColor: '#ffffff',
    elevation: 4,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    zIndex: 3,
  },
  title: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  titleText: {
    flex: 1,
    fontWeight: 'bold',
    marginRight: 16,
  },
  titleActions: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleActionItem: {
    marginLeft: 16,
  },
  imgWrap: {
  },
  playWrap: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 2,
  },
  play: {
    width: 40,
    height: 40,
    paddingTop: 2,
    paddingLeft: 4,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00000090',
  },
  description: {
    padding: 16,
  },
  footer: {
    paddingHorizontal: 16,
    paddingBottom: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  timeDistance: {
    color: '#00000070',
  },
  commentCount: {
    color: 'rgb(234, 83, 102)',
  },
  playlistBar: {
    backgroundColor: '#ffffff',
    height: 16,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    elevation: 4,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#00000005',
  }
});

export default FeedYoutube;
