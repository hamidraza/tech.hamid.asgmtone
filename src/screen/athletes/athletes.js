import React from 'react';
import {View, Text} from 'react-native';

const AthletesScreen = () => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>AthletesScreen</Text>
    </View>
  );
}

AthletesScreen.navigationOptions = {
  title: 'Athletes',
};

export default AthletesScreen;
