import React from 'react';
import {View, Text} from 'react-native';

const MoreScreen = () => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>More Screen</Text>
    </View>
  );
};

MoreScreen.navigationOptions = {
  title: 'More',
};

export default MoreScreen;
