import React from 'react';
import {View, Text} from 'react-native';

const NotificationsScreen = () => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Notifications Screen</Text>
    </View>
  );
};

NotificationsScreen.navigationOptions = {
  title: 'Notifications',
};

export default NotificationsScreen;
