import React from 'react';
import {View, Text} from 'react-native';

const EventsScreen = () => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>EventsScreen</Text>
    </View>
  );
}

EventsScreen.navigationOptions = {
  title: 'Events',
};

export default EventsScreen;
