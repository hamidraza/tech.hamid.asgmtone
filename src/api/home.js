import { handleResponse } from "src/helpers/fetch";

const APIV4 = 'https://app.onefc.com/api/v4';

export const GetFeed = () => {
  return fetch(`${APIV4}/lpfeed`).then(res => handleResponse(res));
};

export const GetEvent = () => {
  return fetch(`${APIV4}/event`).then(res => handleResponse(res));
}

export const GetEvents = () => {
  return fetch(`${APIV4}/events`).then(res => handleResponse(res));
}
