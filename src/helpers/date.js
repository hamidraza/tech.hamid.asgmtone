export const getTimeLeft = (msec) => {
  const days = Math.floor(msec / (1000 * 60 * 60 * 24));
  msec = msec - (days * 1000 * 60 * 60 * 24);
  const hours = Math.floor(msec / (1000 * 60 * 60));
  msec = msec - (hours * 1000 * 60 * 60);
  const minutes = Math.floor(msec / (1000 * 60));
  msec = msec - (minutes * 1000 * 60);
  const seconds = Math.floor(msec / 1000);

  return {
    seconds: `${seconds < 10 ? 0:''}${seconds}`,
    minutes: `${minutes < 10 ? 0:''}${minutes}`,
    hours: `${hours < 10 ? 0:''}${hours}`,
    days: `${days < 10 ? 0:''}${days}`,
  };
};
