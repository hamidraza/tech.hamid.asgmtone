/**
 * Helps to handle response
 * @param {*} res
 */
export const handleResponse = (res): Promise => {
  return new Promise((resolve, reject) => {
    if (!res.ok) {
      if (res.status == 502) {
        reject({
          status: 502,
          error: 'Bad Gateway',
          res,
        });
      } else if (res.status == 503) {
        reject({
          status: 503,
          error: 'Service Unavailable',
          res,
        });
      } else {
        res.json()
          .then(data => reject({...data, res}))
          .catch(err => reject({...err, res}));
      }
    } else {
      res.json().then(data => resolve(data))
        .catch(err => reject({...err, res}));
    }
  });
};

/**
 * get title and message body from Error object
 * @param {string} err Error object
 * @param {string} defaultMsg default message
 * @param {string} defaultTitle default title
 */
export const error = (
  err: Error,
  defaultMsg: string = '',
  defaultTitle: string = 'Error',
): {title: string, msg: string} => {
  let msg = defaultMsg;
  let title = defaultTitle;

  if (err.errors) {
    msg = err.errors.join('\n');
  } else if (err.message || err.msg) {
    if (err.error_title) {
      title = err.error_title;
    } else if (err.code || err.error_code) {
      title = (err.code || err.error_code) + '';
    }
    msg = (err.message || err.msg);
  } else if (err.error) {
    title = err.status + '';
    msg = err.error;
  } else if (err.code && err.details) {
    title = err.details.message;
    msg = err.code;
  } else {
    // Sentry.captureException(err);
  }

  return {
    title,
    msg,
  };
};
