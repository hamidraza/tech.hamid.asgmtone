import React, {useReducer} from 'react';
import {AppRegistry} from 'react-native';
import AppStack from 'src/stack';
import {name as appName} from './app.json';
import OneContext from 'src/context.js';
import reducer, { intialState } from 'src/reducer.js';

const App = () => {
  const [state, dispatch] = useReducer(reducer, intialState);
  return (
    <OneContext.Provider value={{state, dispatch}}>
      <AppStack />
    </OneContext.Provider>
  );
};

AppRegistry.registerComponent(appName, () => App);
